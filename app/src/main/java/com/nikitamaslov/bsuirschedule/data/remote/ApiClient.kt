package com.nikitamaslov.bsuirschedule.data.remote

import com.nikitamaslov.bsuirschedule.BuildConfig
import com.nikitamaslov.bsuirschedule.data.model.Employee
import com.nikitamaslov.bsuirschedule.data.model.Group
import com.nikitamaslov.bsuirschedule.data.model.Schedule
import com.nikitamaslov.bsuirschedule.data.remote.response.LastUpdateDateResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiClient {

    @GET(BuildConfig.ALL_GROUPS_ENDPOINT)
    fun queryAllGroups(): Single<List<Group>>

    @GET(BuildConfig.ALL_EMPLOYEES_ENDPOINT)
    fun queryALlEmployees(): Single<List<Employee>>

    @GET(BuildConfig.GROUP_SCHEDULE_ENDPOINT)
    fun queryGroupSchedule(
        @Query(BuildConfig.GROUP_SCHEDULE_QUERY_PARAMETER) id: Int
    ): Single<Schedule>

    @GET(BuildConfig.EMPLOYEE_SCHEDULE_ENDPOINT)
    fun queryEmployeeSchedule(
        @Query(BuildConfig.EMPLOYEE_SCHEDULE_QUERY_PARAMETER) id: Int
    ): Single<Schedule>

    @GET(BuildConfig.GROUP_SCHEDULE_LAST_UPDATE_DATE_ENDPOINT)
    fun queryGroupScheduleLastUpdateDate(
        @Query(BuildConfig.GROUP_SCHEDULE_LAST_UPDATE_DATE_QUERY_PARAMETER) id: Int
    ): Single<LastUpdateDateResponse>

    //1..4
    @GET(BuildConfig.CURRENT_WEEK_ENDPOINT)
    fun queryCurrentWeek(): Single<Int>

}