package com.nikitamaslov.bsuirschedule.data.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.nikitamaslov.bsuirschedule.data.model.Employee
import com.nikitamaslov.bsuirschedule.data.model.Group
import com.nikitamaslov.bsuirschedule.data.model.ScheduleInfo
import com.nikitamaslov.bsuirschedule.data.model.ScheduleItem

@Database(
    entities = [
        Group::class,
        Employee::class,
        ScheduleItem::class,
        ScheduleInfo::class
    ],
    version = 1,
    exportSchema = false
)
@TypeConverters(DatabaseTypeConverters::class)
abstract class Database : RoomDatabase() {

    abstract fun hostDao(): HostDao

    abstract fun scheduleDao(): ScheduleDao

}