package com.nikitamaslov.bsuirschedule.data.local.database

import androidx.room.Dao
import androidx.room.Insert
import com.nikitamaslov.bsuirschedule.data.model.ScheduleInfo
import com.nikitamaslov.bsuirschedule.data.model.ScheduleItem

@Dao
interface ScheduleDao {

    @Insert
    fun insert(obj: ScheduleItem)

    @Insert
    fun insert(vararg obj: ScheduleItem)

    @Insert
    fun insert(obj: ScheduleInfo)

    @Insert
    fun insert(vararg obj: ScheduleInfo)

}