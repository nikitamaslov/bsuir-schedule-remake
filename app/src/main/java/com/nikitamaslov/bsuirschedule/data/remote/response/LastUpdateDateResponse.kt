package com.nikitamaslov.bsuirschedule.data.remote.response

data class LastUpdateDateResponse(val lastUpdateDate: String?)