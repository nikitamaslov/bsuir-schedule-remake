package com.nikitamaslov.bsuirschedule.data.local.database

import androidx.room.Dao
import androidx.room.Insert
import com.nikitamaslov.bsuirschedule.data.model.Employee
import com.nikitamaslov.bsuirschedule.data.model.Group

@Dao
interface HostDao {

    @Insert
    fun insert(obj: Group)

    @Insert
    fun insert(vararg obj: Group)

    @Insert
    fun insert(obj: Employee)

    @Insert
    fun insert(vararg obj: Employee)

}