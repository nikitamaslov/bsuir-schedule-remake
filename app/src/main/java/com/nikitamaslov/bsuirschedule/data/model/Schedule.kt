package com.nikitamaslov.bsuirschedule.data.model

import androidx.room.Embedded
import androidx.room.Relation
import com.nikitamaslov.bsuirschedule.util.SCHEDULE_TYPE_COMMON
import com.nikitamaslov.bsuirschedule.util.SCHEDULE_TYPE_EXAM

data class Schedule (
    @Embedded val info: ScheduleInfo,
    @Relation(
        entity = ScheduleItem::class,
        parentColumn = "hostId",
        entityColumn = "hostId"
    )
    val allSchedules: List<ScheduleItem>
) {
    val schedules by lazy { allSchedules.filter { it.scheduleType == SCHEDULE_TYPE_COMMON } }
    val examSchedules by lazy { allSchedules.filter { it.scheduleType == SCHEDULE_TYPE_EXAM } }
}