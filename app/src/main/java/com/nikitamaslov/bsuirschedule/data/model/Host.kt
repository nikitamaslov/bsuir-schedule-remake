package com.nikitamaslov.bsuirschedule.data.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

sealed class Host

@JsonClass(generateAdapter = true)
@Entity(indices = [Index("id", unique = true)])
data class Employee(
    @PrimaryKey
    @Json(name = "id")
    val id: Int,
    @Json(name = "academicDepartment") val department: List<String>,
    @Json(name = "fio") val fio: String?,
    @Json(name = "firstName") val firstName: String?,
    @Json(name = "lastName") val lastName: String?,
    @Json(name = "middleName") val middleName: String?,
    @Json(name = "photoLink") val photoLink: String?,
    @Json(name = "rank") val rank: String?
) : Host()

@JsonClass(generateAdapter = true)
@Entity(indices = [Index("id", unique = true)])
data class Group(
    @PrimaryKey
    @Json(name = "id")
    val id: Int,
    @Json(name = "course") val course: Int,
    @Json(name = "name") val name: String
) : Host()