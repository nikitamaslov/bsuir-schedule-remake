package com.nikitamaslov.bsuirschedule.data.remote.response

import com.nikitamaslov.bsuirschedule.data.model.Employee
import com.nikitamaslov.bsuirschedule.data.model.Group

data class ScheduleResponse(
    val employee: Employee?,
    val examSchedules: List<Nested1>?,
    val schedules: List<Nested1>?,
    val studentGroup: Group?
) {

    data class Nested1(
        val schedule: List<Nested2>?,
        val weekDay: String?
    )

    data class Nested2(
        val auditory: List<String>?,
        val employee: List<Employee>?,
        val endLessonTime: String?,
        val lessonTime: String?,
        val lessonType: String?,
        val note: String?,
        val numSubgroup: Int?,
        val startLessonTime: String?,
        val studentGroup: List<String>?,
        val subject: String?,
        val weekNumber: List<Int>?
    )

}