package com.nikitamaslov.bsuirschedule.data.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(indices = [Index("hostId", unique = true)])
data class ScheduleInfo(
    @PrimaryKey val hostId: Int,
    val hostType: Int,
    val lastUpdateDate: String?
)