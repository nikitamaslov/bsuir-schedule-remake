package com.nikitamaslov.bsuirschedule.data.local.database

import androidx.room.TypeConverter

class DatabaseTypeConverters {

    @TypeConverter
    fun fromIntList(src: List<Int>): String = src.joinToString(SEPARATOR)

    @TypeConverter
    fun toIntList(json: String): List<Int> = json.split(SEPARATOR).map(String::toInt)

    @TypeConverter
    fun toStringList(json: String): List<String> = json.split(SEPARATOR)

    @TypeConverter
    fun fromStringList(src: List<String>): String? = src.joinToString(SEPARATOR)

    private companion object {
        const val SEPARATOR = ","
    }

}