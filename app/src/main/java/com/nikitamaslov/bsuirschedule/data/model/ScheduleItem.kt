package com.nikitamaslov.bsuirschedule.data.model

import androidx.room.*

@Entity(
    foreignKeys = [ForeignKey(
        entity = ScheduleInfo::class,
        parentColumns = ["hostId"],
        childColumns = ["hostId"],
        onDelete = ForeignKey.CASCADE
    )],
    indices = [Index("hostId", unique = true)]
)
@SuppressWarnings(
    RoomWarnings.PRIMARY_KEY_FROM_EMBEDDED_IS_DROPPED,
    RoomWarnings.INDEX_FROM_EMBEDDED_ENTITY_IS_DROPPED
)
data class ScheduleItem (
    val hostId: Int,
    val hostType: Int,
    val scheduleType: Int,
    var dayOfWeek: String,
    var startTime: String,
    var endTime: String,
    var subgroup: Int,
    var hidden: Boolean,
    var numberOfWeek: List<Int>,
    var auditory: List<String>,
    var subject: String?,
    var lessonType: String?,
    var note: String?,
    var studentGroups: List<String>,
    @Embedded(prefix = "employee_")
    var employee: Employee?
) {
    @PrimaryKey(autoGenerate = true) var id: Int = 0
}