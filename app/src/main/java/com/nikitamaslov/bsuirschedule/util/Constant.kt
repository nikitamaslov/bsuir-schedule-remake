package com.nikitamaslov.bsuirschedule.util

const val HOST_TYPE_GROUP = 1
const val HOST_TYPE_EMPLOYEE = 2

const val SCHEDULE_TYPE_COMMON = 3
const val SCHEDULE_TYPE_EXAM = 4

const val LAST_UPDATE_DATE_FORMAT = "dd.MM.yyyy"
const val LESSON_TIME_FORMAT = "HH:mm"

const val DATABASE_NAME = "wonderful_database.db"