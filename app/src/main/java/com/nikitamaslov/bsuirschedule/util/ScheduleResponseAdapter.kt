package com.nikitamaslov.bsuirschedule.util

import com.nikitamaslov.bsuirschedule.data.model.Schedule
import com.nikitamaslov.bsuirschedule.data.model.ScheduleInfo
import com.nikitamaslov.bsuirschedule.data.model.ScheduleItem
import com.nikitamaslov.bsuirschedule.data.remote.response.ScheduleResponse
import com.squareup.moshi.FromJson
import javax.inject.Inject

class ScheduleResponseAdapter @Inject constructor() {

    @FromJson
    @Throws(Exception::class)
    fun map(response: ScheduleResponse): Schedule = with(response) {
        val hostId: Int
        val hostType: Int
        when {
            employee != null && studentGroup == null -> {
                hostId = employee.id
                hostType = HOST_TYPE_EMPLOYEE
            }
            studentGroup != null && employee == null -> {
                hostId = studentGroup.id
                hostType = HOST_TYPE_GROUP
            }
            else -> throw Exception("both 'employee' and 'studentGroup' exist or both null in object: $this")
        }
        val scheduleInfo = ScheduleInfo(hostId, hostType, DateTimeUtil.now(LAST_UPDATE_DATE_FORMAT))
        val schedules = this.schedules?.map { wrapper ->
            wrapper.schedule?.map {
                ScheduleItem(
                    hostId = hostId,
                    hostType = hostType,
                    scheduleType = SCHEDULE_TYPE_COMMON,
                    dayOfWeek = wrapper.weekDay ?: throw Exception("'weekDay' was null"),
                    startTime = it.startLessonTime ?: throw Exception("'startLessonTime' was null"),
                    endTime = it.endLessonTime ?: throw Exception("'startLessonTime' was null"),
                    subgroup = it.numSubgroup ?: DEFAULT_SUBGROUP,
                    hidden = false,
                    numberOfWeek = it.weekNumber ?: DEFAULT_NUMBER_OF_WEEK,
                    auditory = it.auditory ?: emptyList(),
                    subject = it.subject,
                    lessonType = it.lessonType,
                    note = it.note,
                    studentGroups = it.studentGroup ?: emptyList(),
                    employee = it.employee?.firstOrNull()
                )
            } ?: emptyList()
        }?.flatten() ?: emptyList()
        val examSchedules = this.examSchedules?.map { wrapper ->
            wrapper.schedule?.map {
                ScheduleItem(
                    hostId = hostId,
                    hostType = hostType,
                    scheduleType = SCHEDULE_TYPE_EXAM,
                    dayOfWeek = wrapper.weekDay ?: throw Exception("'weekDay' was null"),
                    startTime = it.startLessonTime ?: throw Exception("'startLessonTime' was null"),
                    endTime = it.endLessonTime ?: throw Exception("'startLessonTime' was null"),
                    subgroup = it.numSubgroup ?: DEFAULT_SUBGROUP,
                    hidden = false,
                    numberOfWeek = it.weekNumber ?: DEFAULT_NUMBER_OF_WEEK,
                    auditory = it.auditory ?: emptyList(),
                    subject = it.subject,
                    lessonType = it.lessonType,
                    note = it.note,
                    studentGroups = it.studentGroup ?: emptyList(),
                    employee = it.employee?.firstOrNull()
                )
            } ?: emptyList()
        }?.flatten() ?: emptyList()
        return Schedule(scheduleInfo, schedules + examSchedules)
    }

    companion object {
        private const val DEFAULT_SUBGROUP = 0
        private val DEFAULT_NUMBER_OF_WEEK = listOf(0,1,2,3,4)
    }

}