package com.nikitamaslov.bsuirschedule.util.rx

import io.reactivex.Scheduler

interface Schedulers {

    fun ui(): Scheduler

    fun io(): Scheduler

    fun computation(): Scheduler

}