package com.nikitamaslov.bsuirschedule.util.rx

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers as RxSchedulers

class Schedulers_Impl : Schedulers {

    override fun ui(): Scheduler {
        return AndroidSchedulers.mainThread()
    }

    override fun io(): Scheduler {
        return RxSchedulers.io()
    }

    override fun computation(): Scheduler {
        return RxSchedulers.computation()
    }
}