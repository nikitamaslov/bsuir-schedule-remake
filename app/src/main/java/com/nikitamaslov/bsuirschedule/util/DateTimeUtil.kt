package com.nikitamaslov.bsuirschedule.util

import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtil {

    fun now(pattern: String): String {
        val sdf = SimpleDateFormat(pattern, Locale.getDefault())
        val currentDate = Date()
        return sdf.format(currentDate)
    }

}