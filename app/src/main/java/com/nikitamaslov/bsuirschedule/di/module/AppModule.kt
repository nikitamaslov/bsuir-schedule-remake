package com.nikitamaslov.bsuirschedule.di.module

import androidx.room.Room
import com.nikitamaslov.bsuirschedule.App
import com.nikitamaslov.bsuirschedule.BuildConfig
import com.nikitamaslov.bsuirschedule.data.local.database.Database
import com.nikitamaslov.bsuirschedule.data.remote.ApiClient
import com.nikitamaslov.bsuirschedule.util.DATABASE_NAME
import com.nikitamaslov.bsuirschedule.util.ScheduleResponseAdapter
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
class AppModule {



    /*
    * database providers
    */

    @Singleton
    @Provides
    fun provideDatabase(app: App): Database = Room
        .databaseBuilder(app, Database::class.java, DATABASE_NAME)
        .build()

    /*
    * api providers
    */

    @Singleton
    @Provides
    fun provideApiClient(retrofit: Retrofit): ApiClient {
        return retrofit.create(ApiClient::class.java)
    }

    @Provides
    fun provideRetrofit(client: OkHttpClient, factory: MoshiConverterFactory): Retrofit {
        return Retrofit
            .Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(factory)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(client)
            .build()
    }

    @Provides
    fun provideConverterFactory(adapter: ScheduleResponseAdapter): MoshiConverterFactory {
        val moshi = Moshi.Builder()
            .add(adapter)
            .build()
        return MoshiConverterFactory.create(moshi)
    }

    @Provides
    fun provideOkHttpClient(interceptor: HttpLoggingInterceptor): OkHttpClient {
        return OkHttpClient
            .Builder()
            .apply {
                if (BuildConfig.DEBUG) addInterceptor(interceptor)
            }
            .build()
    }

    @Provides
    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor()
            .apply { level = HttpLoggingInterceptor.Level.BODY }
    }

}