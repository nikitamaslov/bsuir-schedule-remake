package com.nikitamaslov.bsuirschedule.di.component

import com.nikitamaslov.bsuirschedule.App
import com.nikitamaslov.bsuirschedule.di.builder.ActivityBuilder
import com.nikitamaslov.bsuirschedule.di.module.AppModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    AndroidSupportInjectionModule::class,
    AppModule::class,
    ActivityBuilder::class
])
interface AppComponent : AndroidInjector<App> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()
}