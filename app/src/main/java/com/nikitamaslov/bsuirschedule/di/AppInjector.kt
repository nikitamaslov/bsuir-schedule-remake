package com.nikitamaslov.bsuirschedule.di

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.nikitamaslov.bsuirschedule.App
import com.nikitamaslov.bsuirschedule.di.component.DaggerAppComponent
import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector

object AppInjector {

    fun inject(app: App) =
        DaggerAppComponent
            .builder()
            .create(app)
            .inject(app)
            .also { processApplication(app) }

    private fun processApplication(app: App) {
        app.registerActivityLifecycleCallbacks(
            object : Application.ActivityLifecycleCallbacks {
                override fun onActivityCreated(
                    activity: Activity?,
                    savedInstanceState: Bundle?
                ) = processActivity(activity)
                override fun onActivityStarted(activity: Activity?) = Unit
                override fun onActivityResumed(activity: Activity?) = Unit
                override fun onActivityPaused(activity: Activity?) = Unit
                override fun onActivityStopped(activity: Activity?) = Unit
                override fun onActivitySaveInstanceState(
                    activity: Activity?,
                    outState: Bundle?
                ) = Unit
                override fun onActivityDestroyed(activity: Activity?) = Unit
            }
        )
    }

    private fun processActivity(activity: Activity?) {
        if (activity == null) return
        if (activity is Injectable || activity is HasSupportFragmentInjector) {
            AndroidInjection.inject(activity)
        }
        if (activity is FragmentActivity && activity is HasSupportFragmentInjector) {
            activity.supportFragmentManager
                .registerFragmentLifecycleCallbacks(
                    object : FragmentManager.FragmentLifecycleCallbacks() {
                        override fun onFragmentCreated(
                            fm: FragmentManager,
                            f: Fragment,
                            savedInstanceState: Bundle?
                        ) {
                            if (f is Injectable) {
                                AndroidSupportInjection.inject(f)
                            }
                        }
                    }, true
                )
        }
    }

}